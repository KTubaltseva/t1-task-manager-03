# TASK MANAGER

## DEVELOPER
**NAME:** Tubaltseva Ksenia

**E-MAIL:** ktubaltseva@t1-consulting.ru

## SOFTWARE
**OS:** Windows 10

**JDK:** OpenJDK 1.8.0_322

## HARDWARE
**CPU:** i5

**RAM:** 8Gb

**SSD:** 256Gb

## RUN APPLICATION
````shell
java -jar ./task-manager.jar
````